from django.contrib import admin
from django.utils.html import format_html_join

from akademik.models import (
    ProgramStudi,
    Matakuliah,
    RuangKuliah,
    Jadwal,
    KRS,
    KHS,
    Nilai
)

# Program studi / prodi Admin
@admin.register(ProgramStudi)
class ProgramStudiAdmin(admin.ModelAdmin):
    empty_value_display = 'belum ada nilai'
    
    fieldsets = (
        (None, {
            'fields': (
                ('nama', 'singkat'),
                'ketua',
                ('nik', 'akreditasi'),
            )
        }),
    )

    list_display = (
        'id', 'nama', 'singkat',
        'ketua', 'nik', 'akreditasi', 
        'dibuat_pada', 'diperbarui_pada',
    )

    list_display_links = (
        'nama', 'singkat',
    )

    list_filter = (
        'akreditasi', 'ketua',
    )

    search_fields = (
        'nama', 'singkat',
    )


# Matakuliah Admin
@admin.register(Matakuliah)
class MatakuliahAdmin(admin.ModelAdmin):
    empty_value_display = 'belum ada nilai'
    
    fieldsets = (
        (None, {
            'fields': (
                ('kode',),
                ('nama', 'sks', 'aktif'),
                ('semester', 'program_studi'),
            )
        }),
    )

    list_display = (
        'kode',
        'nama', 
        'sks', 
        'Semester',
        'aktif',
        'Program_studi',
        'dibuat_pada', 
        'diperbarui_pada',
    )

    list_display_links = (
        'kode', 'nama',
    )

    list_filter = (
        'sks', 'program_studi', 'aktif',
        'paritas',
    )

    search_fields = (
        'kode', 'nama',
    )

    def Semester(self, obj):
        return '{} | {}'.format(
            obj.semester,
            obj.get_paritas_display() if obj.paritas else '-'
        )
    
    def Program_studi(self, obj):
        return obj.program_studi.singkat


# Ruang Kuliah Admin
@admin.register(RuangKuliah)
class RuangKuliahAdmin(admin.ModelAdmin):
    empty_value_display = 'belum ada nilai'

    fieldsets = (
        (None, {
            'fields': (
                ('kode', 'nama_ruangan'),
            )
        }),
    )

    list_display = (
       'kode', 'nama_ruangan',
    )

    list_display_links = (
        'kode', 'nama_ruangan',
    )

    search_fields = (
        'kode', 'nama_ruangan',
    )


# Jadwal Admin
@admin.register(Jadwal)
class JadwalAdmin(admin.ModelAdmin):
    empty_value_display = 'belum ada nilai'
    
    fieldsets = (
        (None, {
            'fields': (
                ('matakuliah', 'tahun_akademik'),
                'dosen',
            ),
        }),
        ('Hari, Waktu & Tempat', {
            'fields': (
                ('hari', 'ruang_kuliah'),
                ('jam_mulai', 'jam_akhir')
            )
        })
    )

    list_display = (
        'id',
        'hari',
        'kode',
        'Matakuliah',
        'sks',
        'semester',
        'tahun_akademik',
        'Dosen',
        'waktu',
        'ruang_kuliah',
        'prodi',
        'Mahasiswa',
    )

    list_display_links = (
        'id', 'Matakuliah',
    )

    list_filter = (
        'dosen__nama_lengkap',
        'hari',
        'matakuliah__nama',
    )

    def kode(self,obj):
        return obj.matakuliah.kode if obj.matakuliah else '?'

    def Matakuliah(self, obj):
        return obj.matakuliah.nama if obj.matakuliah else '?'
    
    def sks(self, obj):
        return obj.matakuliah.sks if obj.matakuliah else '?'

    def semester(self, obj):
        return obj.matakuliah.get_semester_display() if obj.matakuliah else '?'
    
    def Dosen(self, obj):
        return ' / '.join([i.nama_lengkap for i in obj.dosen.all()])

    def waktu(self, obj):
        return '%s - %s' % (obj.jam_mulai, obj.jam_akhir)
    
    def prodi(self, obj):
        return obj.matakuliah.program_studi if obj.matakuliah else '?'

    def Mahasiswa(self, obj):
        return '{} Mahasiswa'.format(obj.krs_set.count() if obj.krs_set.exists() else 0)


# KRS
@admin.register(KRS)
class KRSAdmin(admin.ModelAdmin):
    empty_value_display = 'belum ada nilai'
    fieldsets = (
        (None, {
            'fields': (
                'mahasiswa',
                ('jadwal',),
                ('acc_dosen', 'status'),
                ('tahun_akademik',),
            )
        }),
    )

    # list_display = (
    #     'Nim',
    #     'Mahasiswa', 
    #     'Hari',
    #     'Matakuliah', 
    #     'Sks',
    #     'Semester',
    #     'Waktu',
    #     'Dosen',
    #     'Ruang',
    #     'Prodi',
    # )

    # def Nim(self, obj):
    #     return obj.mahasiswa.nim

    # def Mahasiswa(self, obj):
    #     return obj.mahasiswa.nama_lengkap
    
    # def Matakuliah(self, obj):
    #     return format_html_join(
    #         '\n', "<li>{}</li>",
    #         ((
    #             i.matakuliah.nama if i.matakuliah else '?',
    #         ) for i in obj.jadwal_matakuliah.all())
    #     )

    # def Hari(self, obj):
    #     return format_html_join(
    #         '\n', "<li>{}</li>",
    #         ((
    #             jadwal.get_hari_display(),
    #         ) for jadwal in obj.jadwal_matakuliah.all())
    #     )
    
    # def Sks(self, obj):
    #     return format_html_join(
    #         '\n', "<li>{}</li>",
    #         ((
    #             jadwal.matakuliah.sks if jadwal.matakuliah else '?',
    #         ) for jadwal in obj.jadwal_matakuliah.all())
    #     )
    
    # def Semester(self, obj):
    #     return format_html_join(
    #         '\n', "<li>{}</li>",
    #         ((
    #             jadwal.matakuliah.semester if jadwal.matakuliah else '?',
    #         ) for jadwal in obj.jadwal_matakuliah.all())
    #     )
    
    # def Waktu(self, obj):
    #     return format_html_join(
    #         '\n', "<li>{} - {}</li>",
    #         ((
    #             jadwal.jam_mulai,
    #             jadwal.jam_akhir,
    #         ) for jadwal in obj.jadwal_matakuliah.all())
    #     )
    
    # def Dosen(self, obj):
    #     return format_html_join(
    #         '\n', "<li>{}</li>", ((
    #             '/'.join([d.nama_lengkap for d in jadwal.dosen.all()]),
    #         ) for jadwal in obj.jadwal_matakuliah.all())
    #     )

    # def Ruang(self, obj):
    #     return format_html_join(
    #         '\n', "<li>{}</li>", ((
    #             jadwal.ruang_kuliah.kode,
    #         ) for jadwal in obj.jadwal_matakuliah.all())
    #     )
    
    # def Prodi(self, obj):
    #     return format_html_join(
    #         '\n', "<li>{}</li>", ((
    #             jadwal.matakuliah.program_studi.singkat
    #             if jadwal.matakuliah else '?',
    #         ) for jadwal in obj.jadwal_matakuliah.all())
    #     )

    # search_fields = (
    #     'mahasiswa__nim',
    #     'mahasiswa__nama_lengkap',
    #     'jadwal_matakuliah__matakuliah__nama',
    #     'jadwal_matakuliah__dosen__nama_lengkap',
    # )

    # list_display_links = (
    #     'Nim',
    #     'Mahasiswa',
    #     'Matakuliah',
    # )

    # list_filter = (
    #     'jadwal_matakuliah__matakuliah__program_studi',
    #     'jadwal_matakuliah__ruang_kuliah',
    #     'jadwal_matakuliah__jam_mulai',
    #     'jadwal_matakuliah__matakuliah__semester',
    #     'jadwal_matakuliah__matakuliah__sks',
    #     'jadwal_matakuliah__hari',
    #     'status',
    # )

    readonly_fields = (
        'tahun_akademik',
    )


class NilaiInlines(admin.StackedInline):
    model = Nilai
    # fields = (
    #     'khs',
    #     'tugas',
    #     'uts',
    #     'uas',
    #     'total',
    #     'dibuat_oleh',
    # )

@admin.register(KHS)
class KHSAdmin(admin.ModelAdmin):

    fields = (
        'krs',
    )
    def save_model(self, request, obj, form, change):
        if not obj.pk:
            # set dibuat_oleh hanya pada saat pertama kali
            obj.dibuat_oleh = request.user
        obj.diperbarui_oleh = request.user
        super().save_model(request, obj, form, change)

    inlines = (
        NilaiInlines,
    )

    list_display = (
        'NIM',
        'Mahasiswa',
        'Kode',
        'Matakuliah',
        'Sks',
        'Status_ambil',
        'Tugas',
        'Uts',
        'Uas',
        'dibuat_pada',
        'dibuat_oleh',
        'diperbarui_pada',
        'diperbarui_oleh'
    )

    def NIM(self, obj):
        return obj.krs.mahasiswa.nim
    
    def Mahasiswa(self, obj):
        return obj.krs.mahasiswa.nama_lengkap
    
    def Kode(self, obj):
        return obj.krs.jadwal.matakuliah.kode
    
    def Matakuliah(self, obj):
        return obj.krs.jadwal.matakuliah.nama
    
    def Sks(self, obj):
        return obj.krs.jadwal.matakuliah.sks

    def Status_ambil(self, obj):
        return obj.krs.status
    
    def Tugas(self, obj):
        return obj.nilai.tugas if obj.nilai.tugas else '0'
    
    def Uts(self, obj):
        return obj.nilai.uts if obj.nilai.uts else '0'
    
    def Uas(self, obj):
        return obj.nilai.uas if obj.nilai.uas else '0'

    list_display_links = (
        'NIM',
        'Mahasiswa',
    )

    list_filter = (
        'krs__jadwal__matakuliah__nama',
    )

    # def 
# @admin.register(Nilai)
# class NilaiAdmin(admin.ModelAdmin):
    
#     fields = (
#         'krs',
#         'tugas', 
#         'uts',
#         'uas',
#         'total',
#     )

#     def save_model(self, request, obj, form, change):
#         if not obj.pk:
#             # set dibuat_oleh hanya pada saat pertama kali
#             obj.dibuat_oleh = request.user
#         super().save_model(request, obj, form, change)
