from rest_framework import serializers

from akademik.models import Jadwal

class JadwalSerializer(serializers.ModelSerializer):

    class Meta:
        model = Jadwal
        fields = '__all__'