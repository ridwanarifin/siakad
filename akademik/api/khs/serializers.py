from rest_framework import serializers

from akademik.models import KHS

class KhsSerializer(serializers.ModelSerializer):

    class Meta:
        model = KHS
        fields = '__all__'