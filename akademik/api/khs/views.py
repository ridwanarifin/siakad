from rest_framework import viewsets

from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated, DjangoModelPermissions

from akademik.models import KHS
from akademik.api.khs.serializers import KhsSerializer
from rest_framework import filters

class KhsViewSet(viewsets.ModelViewSet):
    authentication_classes = (
        SessionAuthentication,
        JWTAuthentication
    )
    permission_classes = (
        DjangoModelPermissions,
        IsAuthenticated
    )
    serializer_class = KhsSerializer
    filter_backends = [
        filters.SearchFilter,
        filters.OrderingFilter
    ]
    search_fields = [
        'krs__mahasiswa__nama_lengkap', 
        'krs__mahasiswa__nim',
        'krs__jadwal__matakuliah__nama', 
    ]

    def get_queryset(self):
        queryset = KHS.objects.all()
        return queryset
