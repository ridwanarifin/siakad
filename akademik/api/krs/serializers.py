from rest_framework import serializers

from akademik.models import KRS

class KrsSerializer(serializers.ModelSerializer):

    class Meta:
        model = KRS
        fields = '__all__'