from rest_framework import serializers

from akademik.models import Nilai

class NilaiSerializer(serializers.ModelSerializer):

    class Meta:
        model = Nilai
        fields = '__all__'