from rest_framework import viewsets

from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated, DjangoModelPermissions

from akademik.models import Nilai
from akademik.api.nilai.serializers import NilaiSerializer
from rest_framework import filters

class NilaiViewSet(viewsets.ModelViewSet):
    authentication_classes = (
        SessionAuthentication,
        JWTAuthentication
    )
    permission_classes = (
        DjangoModelPermissions,
        IsAuthenticated
    )
    serializer_class = NilaiSerializer
    filter_backends = [
        filters.SearchFilter,
        filters.OrderingFilter
    ]
    search_fields = [
        'khs__krs__tahun_akademik',
        'khs__krs__status',
        'khs__krs__mahasiswa__nim',
        'khs__krs__mahasiswa__nama_lengkap',
        'khs__krs__jadwal__dosen__id',
        'khs__krs__jadwal__dosen__nidn',
        'khs__krs__jadwal__dosen__nama_lengkap',
        'khs__krs__jadwal__matakuliah__kode',
        'khs__krs__jadwal__matakuliah__nama',
        'khs__krs__jadwal__matakuliah__sks',
        'khs__krs__jadwal__matakuliah__paritas',
    ]

    def get_queryset(self):
        queryset = Nilai.objects.all()
        return queryset
