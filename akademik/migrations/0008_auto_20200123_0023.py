# Generated by Django 2.2 on 2020-01-22 17:23

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('akademik', '0007_auto_20200122_2223'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='khs',
            options={'verbose_name_plural': 'KHS'},
        ),
        migrations.AlterModelOptions(
            name='nilai',
            options={'verbose_name_plural': 'Nilai'},
        ),
        migrations.AddField(
            model_name='nilai',
            name='diperbarui_oleh',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='diperbarui_oleh_user', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='nilai',
            name='dibuat_oleh',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='dibuat_oleh_user', to=settings.AUTH_USER_MODEL),
        ),
    ]
