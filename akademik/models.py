from django.db import models
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _
from django.urls import reverse


import datetime

def YEAR_CHOICES():
    return [(y,y) for y in range(2020, datetime.date.today().year+11)]

def CURRENT_YEAR():
    return datetime.date.today().year

HARI = (
    (1, 'Senin'),
    (2, 'Selasa'),
    (3, 'Rabu'),
    (4, 'Kamis'),
    (5, "Jum'at"),
    (6, 'Sabtu'),
)

PARITAS = [
    ('ganjil', 'Ganjil'), ('genap', 'Genap')
]

SEMESTER = [
    (1, '1 (Satu)'), 
    (2, '2 (Dua)'), 
    (3, '3 (Tiga)'), 
    (4, '4 (Empat)'), 
    (5, '5 (Lima)'), 
    (6, '6 (Enam)'), 
    (7, '7 (Tujuh)'), 
    (8, '8 (Delapan)')
]


# Program Studi / Prodi
class ProgramStudi(models.Model):
    nama            = models.CharField(max_length=100)
    singkat         = models.CharField(max_length=10, help_text='Singkatan nama program studi')
    ketua           = models.CharField(max_length=100)
    nik             = models.CharField('NIK', max_length=30, null=True, blank=True)
    akreditasi      = models.CharField(max_length=2)
    dibuat_pada     = models.DateTimeField(auto_now_add=True)
    diperbarui_pada = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        self.nama           = self.nama.title()
        self.singkat        = self.singkat.upper()
        self.ketua          = self.ketua.title()
        self.akreditasi     = self.akreditasi.upper()
        return super().save(*args, **kwargs)

    def __str__(self):
        return self.nama
    
    class Meta:
        ordering = ('id',)
        verbose_name_plural = 'Program Studi'


def default_paritas(args):
    if args % 2:
        return 'ganjil'
    else:
        return 'genap'

# Matakuliah
class Matakuliah(models.Model):
    kode            = models.CharField(max_length=10, primary_key=True)
    nama            = models.CharField(max_length=100)
    sks             = models.PositiveSmallIntegerField(_('SKS'))
    semester        = models.PositiveSmallIntegerField(choices=SEMESTER)
    paritas         = models.CharField(max_length=6, choices=PARITAS)
    aktif           = models.BooleanField(_('status aktif'),default=True)
    program_studi   = models.ForeignKey('ProgramStudi', on_delete=models.CASCADE)
    dibuat_pada     = models.DateTimeField(auto_now_add=True)
    diperbarui_pada = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        self.nama   = self.nama.title()
        self.paritas = default_paritas(self.semester)
        return super(Matakuliah, self).save(*args, **kwargs)

    def __str__(self):
        return 'Kode: {} | Matakuliah: {} | SKS: {} | Semester: {} | Prodi: {}'.format(
            self.kode, 
            self.nama, 
            self.sks, 
            self.get_semester_display(), 
            self.program_studi.singkat,
        )
    
    class Meta:
        ordering            = ('kode',)
        verbose_name_plural = 'Matakuliah'


# Ruang Kuliah
class RuangKuliah(models.Model):
    kode            = models.CharField(max_length=10, primary_key=True)
    nama_ruangan    = models.CharField(max_length=100)

    def save(self, *args, **kwargs):
        self.nama_ruangan = self.nama_ruangan.title()
        return super(RuangKuliah, self).save(*args, **kwargs)
    
    def __str__(self):
        return '%s | %s' % (self.kode, self.nama_ruangan)
    
    class Meta:
        ordering            = ('kode',)
        verbose_name_plural = 'Kelas / Ruang Kuliah'


# Jadwal
class Jadwal(models.Model):
    tahun_akademik  = models.PositiveSmallIntegerField(choices=YEAR_CHOICES(), default=CURRENT_YEAR())
    matakuliah      = models.OneToOneField('Matakuliah', null=True, on_delete=models.SET_NULL)
    dosen           = models.ManyToManyField('dosen.Profile')
    hari            = models.PositiveSmallIntegerField(choices=HARI)
    jam_mulai       = models.TimeField()
    jam_akhir       = models.TimeField()
    tampil          = models.BooleanField(default=True)
    ruang_kuliah    = models.ForeignKey('RuangKuliah', null=True, on_delete=models.SET_NULL)
    dibuat_pada     = models.DateTimeField(auto_now_add=True)
    diperbarui_pada = models.DateTimeField(auto_now=True)

    def __str__(self):
        return 'Hari: %s |Matakuliah: %s |SKS: %s |Semester: %s |Waktu: %s - %s |Dosen: %s |Ruang: %s |Prodi: %s' % (
            self.get_hari_display(), 
            self.matakuliah.nama if self.matakuliah else '?', 
            self.matakuliah.sks if self.matakuliah else '?',
            self.matakuliah.get_semester_display() if self.matakuliah else '?',
            self.jam_mulai, 
            self.jam_akhir, 
            ' / '.join([i.nama_lengkap for i in self.dosen.all()]),
            self.ruang_kuliah,
            self.matakuliah.program_studi.singkat if self.matakuliah else '?'
        )
    
    class Meta:
        ordering            = (
            'matakuliah__semester',
            'hari',
            'jam_mulai',
        )
        verbose_name_plural = 'Jadwal'


# KRS
class KRS(models.Model):
    tahun_akademik  = models.PositiveSmallIntegerField(
        choices=YEAR_CHOICES(), 
        default=CURRENT_YEAR(),
        editable=False
    )
    jadwal              = models.ForeignKey('Jadwal', verbose_name='Jadwal Matakuliah', on_delete=models.CASCADE, null=True, blank=True)
    mahasiswa           = models.ForeignKey('mahasiswa.Profile', on_delete=models.CASCADE)
    acc_dosen           = models.BooleanField(default=True)
    status              = models.CharField(max_length=8, choices=[
                        ('Baru', 'Baru'), ('Remedial', 'Remedial')
                        ], default='Baru')
    dibuat_pada         = models.DateTimeField(auto_now_add=True)
    diperbarui_pada     = models.DateTimeField(auto_now=True)


    def __str__(self):
        return ('Mahasiswa: {} - {} | Matakuliah: {} | Dosen: {} | Status ambil: {}'.format(
        self.mahasiswa.nim,
        self.mahasiswa.nama_lengkap,
        self.jadwal.matakuliah.nama,
        '/'.join([dosen.nama_lengkap for dosen in self.jadwal.dosen.all()]),
        self.status,
    ))

    class Meta:
        ordering            = ('id',)
        verbose_name_plural = 'KRS'


class KHS(models.Model):
    krs = models.OneToOneField(
        'KRS',
        on_delete=models.CASCADE,
        help_text='tidak bisa pilih bila status ambil telah ada'
    )
    dibuat_pada     = models.DateTimeField(auto_now_add=True)
    diperbarui_pada = models.DateTimeField(auto_now=True)
    dibuat_oleh     = models.ForeignKey(
        'core.User', 
        null=True, blank=True, 
        editable=False,
        default=None,
        on_delete=models.SET_NULL,
        related_name='dibuat_oleh_user'
    )
    diperbarui_oleh = models.ForeignKey(
        'core.User', 
        null=True, blank=True, 
        default=None,
        editable=False,
        on_delete=models.SET_NULL,
        related_name='diperbarui_oleh_user'
    )

    def __str__(self):
        return '{} | Matakuliah: {}'.format(
            self.krs.mahasiswa.nama_lengkap,
            self.krs.jadwal.matakuliah.nama,
        )
    
    class Meta:
        verbose_name_plural = 'KHS'


class Nilai(models.Model):
    khs             = models.OneToOneField(
        'KHS',
        null=True,
        on_delete=models.CASCADE
    )
    tugas           = models.FloatField(null=True, blank=True)
    uts             = models.FloatField(null=True, blank=True)
    uas             = models.FloatField(null=True, blank=True)
    total           = models.CharField(
        max_length=2, 
        choices=[
            ('A', 'A'), ('B+', 'B+'), ('B', 'B'), 
            ('C+', 'C+'), ('C', 'C'), ('D', 'D'), ('E', 'E')
        ],
        null=True, blank=True
    )

    def __str__(self):
        return 'Tugas: {} | UTS: {} | UAS: {}'.format(
            self.tugas,
            self.uts,
            self.uas,
        )
    dinilai_pada     = models.DateTimeField(auto_now_add=True)
    diperbarui_pada = models.DateTimeField(auto_now=True)
    
    class Meta:
        verbose_name_plural = 'Nilai'

