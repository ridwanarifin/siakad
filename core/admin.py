from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from .forms import UserCreationForm, UserChangeForm
from .models import User

class CustomUserAdmin(BaseUserAdmin):
    form = UserChangeForm
    add_form = UserCreationForm

    list_display = (
        'username', 'email', 'phone_number',
        'user_type', 'is_staff', 'is_active',
        'avatar', 'date_joined', 
    )

    list_filter = (
        'user_type', 'date_joined'
    )

    search_fields = (
        'username', 'email'
    )

    fieldsets = (
        (None, {
            'fields': (
                'username', 'password', 'email',
                'phone_number', 'avatar'
            )
        }),
        (_('Permissions'), {
            'fields': (
                ('user_type', 'is_active'),
                'is_staff', 'is_superuser', 'user_permissions',
                'groups'
            )
        }),
        (_('Important dates'), {
            'fields': (
                'last_login', 'date_joined'
            )
        })
    )

    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'user_type', 'password1', 'password2')}
        ),
    )

    filter_horizontal = ()

    readonly_fields = (
        'date_joined',
    )

admin.site.register(User, CustomUserAdmin)
