from rest_framework import routers

from core.api.views import UserViewSet
from django.urls import path, include

router = routers.DefaultRouter()
router.register(r'', UserViewSet, basename='User')

urlpatterns = [
    path('', include(router.urls)),
]
