from rest_framework import viewsets
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.http import JsonResponse

from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated, DjangoModelPermissions

from core.models import User
from core.api.serializers import UserSerializer

class UserViewSet(viewsets.ModelViewSet):
    authentication_classes = (SessionAuthentication, JWTAuthentication)
    permission_classes  = (DjangoModelPermissions, IsAuthenticated)
    serializer_class    = UserSerializer

    def get_queryset(self):
        queryset = User.objects.all()
        username = self.request.query_params.get('u', None)
        if username is not None:
            queryset = queryset.filter(username=username)
        return queryset


def validate_username(request):
    username = request.GET.get('username', None)
    print(request.user)
    response = User.objects.filter(username__iexact=username).exists()
    return JsonResponse({ "is_exist": response })

    # username = request.GET.get('username', None)
        # data = {
        #     'is_taken': User.objects.filter(username__exact=username).exists()
        # }
        # return JsonResponse(data)