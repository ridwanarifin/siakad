from django.contrib.auth.models import (
    AbstractBaseUser, AbstractUser, PermissionsMixin
)
from django.db import models
from django.core.mail import send_mail
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField

# from django.contrib.auth.validators import UnicodeUsernameValidator

from .manager import MyUserManager

class User(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.
    Username and password are required. Other fields are optional.
    """
    # username_validator = UnicodeUsernameValidator()

    USER_TYPE = (
        (1, 'Mahasiswa'),
        (2, 'Dosen'),
        (3, 'Admin'),
    )
    
    username = models.CharField(
        _('username'),
        max_length=150,
        unique=True,
        help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        # validators=[username_validator],
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )
    email = models.EmailField(_('email address'), blank=True)
    phone_number = PhoneNumberField(
        _('phone number'),
        blank=True
    )
    user_type = models.PositiveSmallIntegerField(_('user'), choices=USER_TYPE)
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    avatar = models.ImageField(upload_to='avatars/', blank=True, null=True)
    date_joined = models.DateTimeField(
        _('date joined'), 
        auto_now_add=True
    )

    objects = MyUserManager()

    # EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['user_type']

    class Meta:
        db_table = 'auth_user'
        verbose_name = _('user')
        verbose_name_plural = _('users')
    
    def email_user(self, subject, message, from_email=None, **kwargs):
        '''Send mail to this user.'''
        send_mail(subject, message, from_mail, [self.email], **kwargs)