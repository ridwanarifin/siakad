from rest_framework import serializers

from dosen.models import Profile

class DosenSerializer(serializers.ModelSerializer):

    class Meta:
        model = Profile
        fields = '__all__'