from rest_framework import routers

from dosen.api.views import DosenViewSet
from django.urls import path, include

router = routers.DefaultRouter()
router.register(r'', DosenViewSet, basename='dosen')

urlpatterns = router.urls