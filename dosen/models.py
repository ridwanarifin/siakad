from django.db import models
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from django.utils import timezone


AGAMA = [
    ('buddha', 'Buddha'),
    ('hindu', 'Hindu'),
    ('islam', 'Islam'),
    ('konghucu', 'Kong Hu Cu'),
    ('Kristen',(
        ('katolik', 'Katolik'),
        ('protestan', 'Protestan'),
    )),
]

JENIS_KELAMIN = [
    ('L', 'Laki-laki'),
    ('P', 'Perempuan'),
]


def checkNone(args):
    if args is not None:
        return args.title()
    else:
        return


class Profile(models.Model):
    akun            = models.OneToOneField(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.SET_NULL, related_name='akun_dosen')
    program_studi   = models.ForeignKey('akademik.ProgramStudi', null=True, on_delete=models.SET_NULL, related_name='program_studi_dosen')
    nidn            = models.CharField('NIDN', max_length=20)
    nama_lengkap    = models.CharField(max_length=150)
    jenis_kelamin   = models.CharField(max_length=1, choices=JENIS_KELAMIN)
    tempat_lahir    = models.CharField(max_length=40)
    tanggal_lahir   = models.DateField()
    agama           = models.CharField(max_length=9, choices=AGAMA, default='islam')
    kebangsaan      = models.CharField(max_length=20, default="Indonesia")
    alamat          = models.TextField(help_text='nama jalan, nomor rumah', null=True, blank=True)
    rt              = models.PositiveSmallIntegerField(help_text='rukun tetangga', verbose_name='RT', null=True, blank=True)
    rw              = models.PositiveSmallIntegerField(help_text='rukun warga', verbose_name='RW', null=True, blank=True)
    kelurahan       = models.CharField(max_length=30, verbose_name='Kelurahan / Desa', null=True, blank=True)
    kecamatan       = models.CharField(max_length=30, null=True, blank=True)
    kab_kota        = models.CharField(max_length=30, verbose_name='Kabupaten / Kota', null=True, blank=True)
    provinsi        = models.CharField(max_length=30, null=True, blank=True)
    kode_pos        = models.PositiveSmallIntegerField(null=True, blank=True)
    pendidikan      = models.CharField(max_length=30)
    file_foto       = models.ImageField(upload_to='dosen/foto/', blank=True)
    status          = models.CharField(max_length=6, choices=[('Aktif', 'Aktif'), ('Keluar', 'Keluar')], default='Aktif')
    tanggal_masuk   = models.DateField(default=timezone.now)
    slug            = models.SlugField(blank=True, editable=False)
    dibuat_pada     = models.DateTimeField(auto_now_add=True)
    diperbarui_pada = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        self.nama_lengkap   = self.nama_lengkap.title()
        self.tempat_lahir   = checkNone(self.tempat_lahir)
        self.kelurahan      = checkNone(self.kelurahan)
        self.kecamatan      = checkNone(self.kecamatan)
        self.kab_kota       = checkNone(self.kab_kota)
        self.slug           = slugify('%s %s' % (self.nidn, self.nama_lengkap.lower()))

        return super(Profile, self).save(*args, **kwargs)
    
    def __str__(self):
        return '{} | {}'.format(self.id, self.nama_lengkap)
    
    class Meta:
        ordering = ('id',)
        verbose_name = _('dosen')
        verbose_name_plural = _('profile')

