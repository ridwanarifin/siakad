from django.contrib import admin
from django.utils.safestring import mark_safe

from mahasiswa.models import (
    Profile,
    Orangtua,
)

from akademik.models import KRS

def is_empty(obj):
    if obj is not None:
        return obj
    else:
        return '? '

# Register admin form

class KRSInline(admin.StackedInline):
    model = KRS
    extra = 0

class OrangtuaInline(admin.StackedInline):
    model = Orangtua
    extra = 0

@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):

    inlines = (
        OrangtuaInline,
        KRSInline,
    )
    
    fieldsets = (
        (None, {
            'fields': (
                'nama_lengkap',
                ('tempat_lahir', 'tanggal_lahir'),
                ('jenis_kelamin', 'agama', 'kebangsaan'),
                'slug',
            ),
        }),
        ('Akademik', {
            'fields': (
                'nim',
                ('akun', 'file_foto'),
                ('program_studi', 'tanggal_masuk') 
            )
        }),
        ('Alamat Rumah', {
            'fields': (
                ('alamat','rt', 'rw'),
                ('kelurahan', 'kecamatan'),
                ('kab_kota', 'provinsi', 'kode_pos')
            )
        }),
    )

    list_display = (
        'nim',
        'akun',
        'nama_lengkap',
        'alamat_lengkap',
        'program_studi',
        'tanggal_masuk',
    )

    list_display_links = (
        'nim',
        'nama_lengkap',
    )

    list_filter = (
        'program_studi',
        'provinsi',
        'kab_kota',
    )

    readonly_fields = (
        'slug',
        'dibuat_pada',
        'diperbarui_pada',
    )

    search_fields = (
        'nim',
        'nama_lengkap',
    )

    def alamat_lengkap(self, obj):
        return "{}, RT.{} RW.{}, {}, {}, {}, {}, {}".format(
            is_empty(obj.alamat), 
            is_empty(obj.rt),
            is_empty(obj.rw), 
            is_empty(obj.kelurahan), 
            is_empty(obj.kecamatan), 
            is_empty(obj.kab_kota), 
            is_empty(obj.provinsi),
            is_empty(obj.kode_pos)
        )
    alamat_lengkap.empty_value_display = 'belum ada nilai'


@admin.register(Orangtua)
class OrangtuaAdmin(admin.ModelAdmin):
    empty_value_display = 'belum ada nilai'

    fieldsets = (
        (None, {
            'fields': (
                'mahasiswa',
                'nama_lengkap',
                ('hubungan', 'hubungan_lain'),
                ('hp','pekerjaan'),
            )
        }),
    )

    list_display = (
        'id',
        'nama_lengkap',
        'hp',
        'orangtua_dari',
        'hubungan',
        'pekerjaan'
    )

    list_display_links = (
        'nama_lengkap',
    )

    list_filter = (
        'hubungan',
        'mahasiswa',
    )

    search_fields = (
        'nama_lengkap',
        'mahasiswa',
    )

    def orangtua_dari(self, obj):
        mhs = []
        for i in obj.profile.all():
            mhs.append(f'{i.nim} {i.namal_engkap}')
        return mark_safe('<br/>'.join(mhs))
