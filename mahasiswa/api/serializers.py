from rest_framework import serializers

from mahasiswa.models import Profile

class MahasiswaSerializer(serializers.ModelSerializer):
    
    class Meta:
        model  = Profile
        fields = '__all__'

