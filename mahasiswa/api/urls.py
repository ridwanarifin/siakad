from rest_framework import routers

from mahasiswa.api.views import MahasiswaViewSet

router = routers.DefaultRouter()
router.register(r'', MahasiswaViewSet, basename="Mahasiswa")

urlpatterns = router.urls
