from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated, DjangoModelPermissions

from mahasiswa.models import Profile
from mahasiswa.api.serializers import MahasiswaSerializer

class MahasiswaViewSet(viewsets.ModelViewSet):
    authentication_classes = (SessionAuthentication, JWTAuthentication)
    permission_classes  = (DjangoModelPermissions, IsAuthenticated)
    serializer_class = MahasiswaSerializer

    def get_queryset(self):
        print(self.request.user)
        queryset = Profile.objects.all()
        akun = self.request.query_params.get('a', None)
        if akun is not None:
            queryset = queryset.filter(akun=akun)
        return queryset
