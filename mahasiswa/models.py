from django.db import models
from django.utils.text import slugify

from phonenumber_field.modelfields import PhoneNumberField
from django.conf import settings


AGAMA = [
    ('buddha', 'Buddha'),
    ('hindu', 'Hindu'),
    ('islam', 'Islam'),
    ('konghucu', 'Kong Hu Cu'),
    ('Kristen',(
        ('katolik', 'Katolik'),
        ('protestan', 'Protestan'),
    )),
]

JENIS_KELAMIN = [
    ('L', 'Laki-laki'),
    ('P', 'Perempuan'),
]

HUBUNGAN = [
    ('Ayah', 'Ayah'),
    ('Ibu', 'Ibu'),
    ('Lainnya', 'Lainnya'),
]

def checkNone(args):
    if args is not None:
        return args.title()
    else:
        return


class Profile(models.Model):
    """Profil Mahasiswa"""
    akun            = models.OneToOneField(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.SET_NULL, help_text='digunakan untuk login SIA', related_name='akun_mahasiswa')
    tahun_akademik  = models.CharField(max_length=8, null=True, blank=True)
    nim             = models.CharField(primary_key=True, verbose_name='NIM', max_length=9, help_text='nomor induk mahasiswa')
    nama_lengkap    = models.CharField(max_length=225)
    jenis_kelamin   = models.CharField(max_length=1, choices=JENIS_KELAMIN)
    tempat_lahir    = models.CharField(max_length=40)
    tanggal_lahir   = models.DateField()
    agama           = models.CharField(max_length=9, choices=AGAMA, default='islam')
    kebangsaan      = models.CharField(max_length=20, default="Indonesia")
    alamat          = models.TextField(help_text='nama jalan, nomor rumah', null=True, blank=True)
    rt              = models.PositiveSmallIntegerField(help_text='rukun tetangga', verbose_name='RT', null=True, blank=True)
    rw              = models.PositiveSmallIntegerField(help_text='rukun warga', verbose_name='RW', null=True, blank=True)
    kelurahan       = models.CharField(max_length=30, verbose_name='Kelurahan / Desa', null=True, blank=True)
    kecamatan       = models.CharField(max_length=30, null=True, blank=True)
    kab_kota        = models.CharField(max_length=30, verbose_name='Kabupaten / Kota', null=True, blank=True)
    provinsi        = models.CharField(max_length=30, null=True, blank=True)
    kode_pos        = models.PositiveSmallIntegerField(null=True, blank=True)
    program_studi   = models.ForeignKey('akademik.ProgramStudi', null=True, on_delete=models.SET_NULL, related_name='program_studi_mahasiswa')
    tanggal_masuk   = models.DateField()
    status          = models.CharField(choices=[('Aktif', 'Aktif'), ('Lulus', 'Lulus'), ('DO', 'Drop Out')], max_length=5, default='Aktif')
    file_foto       = models.ImageField(upload_to='mahasiswa/foto/', blank=True)
    slug            = models.SlugField(blank=True, editable=False)
    dibuat_pada     = models.DateTimeField(auto_now_add=True)
    diperbarui_pada = models.DateTimeField(auto_now=True)

    def get_absolute_url(self):
        return reverse('mahasiswa-detail', kwargs={'slug': self.slug})

    def save(self, *args, **kwargs):
        self.nama_lengkap   = self.nama_lengkap.title() if self.nama_lengkap is not None else None
        self.tempat_lahir   = checkNone(self.tempat_lahir)
        self.kelurahan      = checkNone(self.kelurahan)
        self.kecamatan      = checkNone(self.kecamatan)
        self.kab_kota       = checkNone(self.kab_kota)
        self.kebangsaan     = checkNone(self.kebangsaan)
        self.slug           = slugify('%s %s' % (self.nim, self.nama_lengkap.lower()))

        return super(Profile, self).save(*args, **kwargs)

    def __str__(self):
        return self.slug

    class Meta:
        ordering = ('-dibuat_pada',)
        verbose_name_plural = 'Profil'


class Orangtua(models.Model):
    """Orangtua / Wali Mahasiswa"""
    mahasiswa       = models.ForeignKey('Profile', on_delete=models.CASCADE)
    nama_lengkap    = models.CharField(max_length=225)
    hubungan        = models.CharField(max_length=7, choices=HUBUNGAN, help_text='Keterangan hubungan keluarga')
    hubungan_lain   = models.CharField(null=True, blank=True, max_length=225, help_text='Opsional. Tuliskan hubungan apabila memilih hubungan lainnya')
    hp              = PhoneNumberField(help_text='Nomor hp aktif')
    pekerjaan       = models.CharField(max_length=225, help_text='e.g Wiraswasta, dan lain-lain.')

    def save(self, *args, **kwargs):
        self.nama_lengkap   = self.nama_lengkap.title()
        if self.hubungan_lain: self.hubungan_lain  = self.hubungan_lain.title()
        self.pekerjaan      = self.pekerjaan.title()

        return super().save(*args, **kwargs)

    def __str__(self):
        return self.nama_lengkap
    
    class Meta:
        ordering            = ('id',)
        verbose_name_plural = 'Orang Tua | Wali'
