# RESTfull Web Service Sistem Informasi Akademik

Implementasi RESTfull Web Service menggunakan :

- [Python3](https://www.python.org/downloads/)
- [Django 2.2](https://www.djangoproject.com/)
- [Psycopg2](https://pypi.org/project/psycopg2/) # Python DB API untuk RDBMS PostgreSQL
- [Django REST framework](https://www.django-rest-framework.org/)
- [Djangorestframework Simplejwt](https://github.com/davesque/django-rest-framework-simplejwt) # JSON Web Token authentication untuk Django REST framework
- [Django phone verify](https://github.com/CuriousLearner/django-phone-verify) # nomor telepon verifikasi dengan pengiriman kode sekuriti via sms 


## Installation

Gunakan package manager [pip](https://pip.pypa.io/en/stable/) buat installasi dependencies.

```bash
pip install -r requirements.txt
```

### Kofigurasi

Ubah beberapa konfigurasi di project ``settings.py`` :

#### Database

Ubah konfigurasi database pada line :

```bash
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'my_db',
        'USER': 'my_user_db',
        'PASSWORD': 'my_password_db',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}
```

Informasi lebih jelas silahkan merujuk ke dokumentasi [django setting-DATABASES](https://docs.djangoproject.com/en/3.0/ref/settings/#std:setting-DATABASES).


#### JSON Web Token

Sesuikan dengan kebutuhan untuk kofig token, berikut default konfig nya :

```bash
SIMPLE_JWT = {
    'ACCESS_TOKEN_LIFETIME': timedelta(minutes=5),
    'REFRESH_TOKEN_LIFETIME': timedelta(days=1),
    'ROTATE_REFRESH_TOKENS': False,
    'BLACKLIST_AFTER_ROTATION': True,

    'ALGORITHM': 'HS512',
    'SIGNING_KEY': SECRET_KEY,
    'VERIFYING_KEY': None,
    'AUDIENCE': None,
    'ISSUER': None,

    'AUTH_HEADER_TYPES': ('Bearer',),
    'USER_ID_FIELD': 'id',
    'USER_ID_CLAIM': 'user_id',

    'AUTH_TOKEN_CLASSES': ('rest_framework_simplejwt.tokens.AccessToken',), 
    'TOKEN_TYPE_CLAIM': 'token_type',

    'JTI_CLAIM': 'jti',

    'SLIDING_TOKEN_REFRESH_EXP_CLAIM': 'refresh_exp',
    'SLIDING_TOKEN_LIFETIME': timedelta(minutes=5),
    'SLIDING_TOKEN_REFRESH_LIFETIME': timedelta(days=1),
}
```

Untuk informasi lebih jelas silahkan merujuk ke dokumentasi [Simple JWT settings](https://github.com/davesque/django-rest-framework-simplejwt#settings)


#### Phone verify

Buat akun di [twilio.com](https://www.twilio.com/), setting up semua nya.
Ubah konfigurasi phone verify berikut :

```bash
PHONE_VERIFICATION = {
    'BACKEND': 'phone_verify.backends.twilio.TwilioBackend',
    'OPTIONS': {
        'SID': 'ACbee725ee4e06e5621a36f35bffe21151',
        'SECRET': '39d9b9f1951c5d120fddddd492531386',
        'FROM': '+19782933574',
        'SANDBOX_TOKEN':'123456',
    },
    'TOKEN_LENGTH': 6,
    'MESSAGE': '\nSelamat datang di {app} ! \nSilahkan gunakan kode security {security_code} untuk melanjutkan proses ganti nomor.',
    'APP_NAME': 'Layanan Verifikasi SIAKAD Mobile',
    'SECURITY_CODE_EXPIRATION_TIME': 180,  # In seconds only
    'VERIFY_SECURITY_CODE_ONLY_ONCE': False,  # If False, then a security code can be used multiple times for verification
}
```

Ubah beberapa ``OPTIONS`` :

    SIID : ganti dengan Account SID pada pembuatan akun twillio sebelumnya.

    SECRET : ganti dengan Auth Token pada pembuatan akun twilio sebelumnya.

Untuk informasi lebih jelas silahkan merujuk ke dokumentasi [twilio docs](https://www.twilio.com/docs).

## Sedikit Screenshot

### Not Authenticated

![403 endpoint get /api/user/](./screenshot/0.png | width=50)
![403 endpoint get /api/user/](./screenshot/1.png | width=50)

### Token

![405 endpoint get /api/token](./screenshot/2.png | width=50)
![200 endpoint post /api/token](./screenshot/3.png | width=50)


## Phone Verify

![405 enpoint get /api/phone_verify/phone/register/](./screenshot/4.png | width=50)
![200 enpoint post /api/phone_verify/phone/register/](./screenshot/5.png | width=50)
![sms security code](./screenshot/sms.png | height=50)


## Catatan

* Selanjutnya untuk melakukan request ke setiap endpoint harus menyertakan ``access token``
* ``Access token`` berlaku sampai 5 menit (sesuai konfig diawal), apabila sudah melewati itu maka harus melakukan ``Refresh token`` untuk mendapatkan token yang baru