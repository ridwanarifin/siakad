from rest_framework.routers import DefaultRouter
from phone_verify.api import VerificationViewSet

router = DefaultRouter(trailing_slash=False)
router.register('phone', VerificationViewSet, basename='phone')

urlpatterns = router.urls