from django.conf import settings

import twilio
from twilio.rest import Client

def send_twilio_message(to_number, body):
    client = Client(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)

    message = client.messages.create(
        body= body,
        from_= '+19782933574',
        to=to_number,
    )
    print(message.sid)
    return message