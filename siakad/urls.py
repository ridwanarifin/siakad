from django.contrib import admin
from django.contrib.auth import views as auth_views

from django.utils.safestring import mark_safe
from django.utils.html import format_html
from django.urls import path, include

from django.conf import settings
from django.conf.urls.static import static

from rest_framework import routers

# api
from siakad.api import views
from rest_framework_simplejwt import views as jwt_views
from core.api import views as views_validate

# akademik
from akademik.api.program_studi.views import ProgramStudiViewSet
from akademik.api.matakuliah.views import MatakuliahViewSet
from akademik.api.ruang_kuliah.views import RuangKuliahViewSet
from akademik.api.jadwal.views import JadwalViewSet
from akademik.api.krs.views import KrsViewSet
from akademik.api.khs.views import KhsViewSet
from akademik.api.nilai.views import NilaiViewSet

router = routers.SimpleRouter()
router.register(r'program_studi', ProgramStudiViewSet, basename='program_studi')
router.register(r'matakuliah', MatakuliahViewSet, basename='matakuliah')
router.register(r'ruang_kuliah', RuangKuliahViewSet, basename='ruang_kuliah')
router.register(r'jadwal', JadwalViewSet, basename='jadwal')
router.register(r'krs', KrsViewSet, basename='krs')
router.register(r'khs', KhsViewSet, basename='khs')
router.register(r'nilai', NilaiViewSet, basename='nilai')


urlpatterns = [
    path('api/', include([
        path('validate_username/', views_validate.validate_username, name='validate'),
        path('phone_verify/', include('siakad.api.phone.urls')),
        path('akademik/', include((router.urls, 'akademik'), namespace='akademik_api')),
        path('user/', include('core.api.urls')),
        path('mahasiswa/', include('mahasiswa.api.urls')),
        path('dosen/', include('dosen.api.urls')),
        path('token/', include([
            # path('', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
            path('', views.MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
            path('refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
            path('verify/', jwt_views.TokenVerifyView.as_view(), name='token_verify'),
        ])),
    ])),
    path('admin/', admin.site.urls),
    # password reset feature
    path(
        'admin/password_reset/',
        auth_views.PasswordResetView.as_view(),
        name='admin_password_reset',
    ),
    path(
        'admin/password_reset/done/',
        auth_views.PasswordResetDoneView.as_view(),
        name='password_reset_done',
    ),
    path(
        'reset/<uidb64>/<token>/',
        auth_views.PasswordResetConfirmView.as_view(),
        name='password_reset_confirm',
    ),
    path(
        'reset/done/',
        auth_views.PasswordResetCompleteView.as_view(),
        name='password_reset_complete',
    ),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

admin.site.site_header = format_html("{}",
    'Administrasi Akademi Keperawatan',
)
admin.site.site_title = "Sistem Informasi Akademik"
